package org.kshrd.spring.homework02.controller;


import org.kshrd.spring.homework02.model.Article;
import org.kshrd.spring.homework02.service.ArticleService;
import org.kshrd.spring.homework02.service.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.util.List;


@Controller
public class Springmain{

    @Autowired
    private FileStorageService fileStorageService;
    private ArticleService articleService;
    @Autowired
    public void setArticleService(ArticleService articleService1){
        this.articleService = articleService1;
    }
    @GetMapping("/")
    public String index(Model modelMap){
        List<Article> articleList = articleService.getAllArticle();
        modelMap.addAttribute("articles",articleList);
        return "index";
    }

    @GetMapping("/newArticle")
    public String newArticle(Model model){
        model.addAttribute("article", new Article());
        return "newArticle";
    }
    @PostMapping("/newArticle")
    public String newArticle(@ModelAttribute Article article, @RequestParam MultipartFile file) throws Exception {
        System.out.println(fileStorageService.saveFile(file));
        String filename = fileStorageService.saveFile(file);
//        article.setId((int)(Math.random() *100));
        article.setImg(filename);
        articleService.createArticle(article);
        return "redirect:/";
    }

    @GetMapping("/{id}/view")
    public String view(Model model, @PathVariable int id){
        Article articles = articleService.findAllArticle(id);
        model.addAttribute("articles",articles);
        return "view";
    }

    @GetMapping("/{id}/delete")
    public String delete(Model model, @PathVariable int id){
        articleService.deleteArticle(id);

        return "redirect:/";
    }
    @GetMapping("/edit")
    public String edit(Model model, @PathVariable int id){
        Article article= articleService.findAllArticle(id);
        model.addAttribute("article",article);
        return "/edit";
    }
    @PostMapping("/{id}/edit")
    public String editArticle(@ModelAttribute Article article, @RequestParam MultipartFile file) throws Exception {
        System.out.println(fileStorageService.saveFile(file));
        String filename = fileStorageService.saveFile(file);
//        article.setId((int)(Math.random() *100));
        article.setImg(filename);
        articleService.createArticle(article);
        return "redirect:/";
    }
}
