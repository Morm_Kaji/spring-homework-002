package org.kshrd.spring.homework02.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Article {
    private int id;
    private String title;
    private String description;
    private String img;

}
