package org.kshrd.spring.homework02.service;

import org.kshrd.spring.homework02.model.Article;

import java.util.List;

public interface ArticleService {
    List<Article> getAllArticle();
    Article findAllArticle(int id);
    boolean createArticle(Article article);
    boolean updateArticle(int id, Article article);
    boolean deleteArticle(int id);
}
