package org.kshrd.spring.homework02.service;

import org.kshrd.spring.homework02.model.Article;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
@Service
public class ArticleServiceImp implements ArticleService {

    private List<Article> articleList = new ArrayList<>();
    @Override
    public List<Article> getAllArticle() {
        return articleList;
    }

    @Override
    public Article findAllArticle(int id) {
        for(Article article: articleList){
            if(article.getId()==id){
                return article;
            }
        }
        return null;
    }

    @Override
    public boolean createArticle(Article article) {

        return articleList.add(article);
    }

    @Override
    public boolean updateArticle(int id, Article article) {
        int index = -1;
        for(Article article1 : articleList){
            if(article1.getId() == id){
                index = article1.getId();
                break;
            }
        }
        articleList.set(index, article);
        return false;
    }

    @Override
    public boolean deleteArticle(int id) {
        int index = -1;

        for(Article article : articleList){
            if(article.getId() == id){
               index = article.getId();
               break;
            }
        }
        articleList.remove(index);
        return  true;
    }
}
