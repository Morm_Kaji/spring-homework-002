package org.kshrd.spring.homework02.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.processing.FilerException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
public class FileStorageServiceImp implements FileStorageService {
    private Path fileStorageLocation;
    @Autowired
    public FileStorageServiceImp(@Value("${file.storage.server.path}") String path){
        fileStorageLocation = Paths.get(path);
    }
    @Override
    public String saveFile(MultipartFile file){
        try {
            String filename = file.getOriginalFilename();
            if (filename.contains((".."))) {
                System.out.println("Error file name");
                return null;
            }
            String[] filepaths = filename.split("\\.");
            String extension = filepaths[1];
            filename = UUID.randomUUID() + "." + extension;
            Path targetLocations = fileStorageLocation.resolve(filename);
            Files.copy(file.getInputStream(), targetLocations, StandardCopyOption.REPLACE_EXISTING);
            return filename;
        }catch(FilerException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
